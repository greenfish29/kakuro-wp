﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Kakuro.WP.Controls
{
    public partial class TotalsCellControl : UserControl
    {
        private App _app = (App)Application.Current;

        public TotalsCellControl(int size, int? rowValue, int? columnValue)
        {
            InitializeComponent();
            DataContext = size;
            Initialize(size, rowValue, columnValue);
        }

        private void Initialize(int size, int? rowValue, int? columnValue)
        {
            // Set visual elements
            Container.Width = Container.Height = size;
            Container.Background = new SolidColorBrush(_app.Settings.Theme.DarkerBackgroundColor);
            Border.BorderBrush = new SolidColorBrush(_app.Settings.Theme.BorderColor);
            RowText.Foreground = ColumnText.Foreground = new SolidColorBrush(_app.Settings.Theme.HeadFontColor);
            RowText.FontSize = ColumnText.FontSize = size / 3;
            // Set total values
            if(rowValue.HasValue) RowText.Text = rowValue.Value.ToString();
            if(columnValue.HasValue) ColumnText.Text = columnValue.Value.ToString();
        }

    }
}
