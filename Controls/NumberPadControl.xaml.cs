﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Kakuro.WP.Controls
{
    public partial class NumberPadControl : UserControl
    {
        public event EventHandler NumberSelected;
        public enum Mode { NUMBER, NOTE }

        public int? SelectedNumber { get; set; }

        private List<string> _selectedNotes = new List<string>(9);
        public List<string> SelectedNotes
        {
            get { return _selectedNotes; }
            set
            {
                _selectedNotes = new List<string>(value);
                SetCheckedNotes();
            }
        }

        private Mode _activeMode = Mode.NUMBER;
        public Mode ActiveMode
        {
            get
            {
                return _activeMode;
            }
            set
            {
                if (value == Mode.NUMBER)
                {
                    NumberPad.Visibility = Visibility.Visible;
                    NotePad.Visibility = Visibility.Collapsed;
                }
                else
                {
                    NumberPad.Visibility = Visibility.Collapsed;
                    NotePad.Visibility = Visibility.Visible;
                }
            }
        }

        public NumberPadControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            SelectedNumber = Int32.Parse(btn.Content.ToString());
            NumberSelected(null, null);
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (ToggleButton)sender;

            if (button.IsChecked.Value)
            {
                SelectedNotes.Add(button.Content.ToString());
            }
            else
            {
                SelectedNotes.Remove(button.Content.ToString());
            }
        }

        private void SetCheckedNotes()
        {
            foreach (object obj in NotePad.Children)
            {
                ToggleButton btn = (ToggleButton)obj;
                btn.IsChecked = SelectedNotes.Contains(btn.Content.ToString()) ? true : false;
            }
        }
    }
}
