﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Kakuro.WP.Controls
{
    public partial class ContentCellControl : UserControl
    {
        private App _app = (App)Application.Current;
        public event EventHandler NumberPadRequested;
        public event EventHandler NotePadRequested;

        private int? _insertedNumber;
        public int? InsertedNumber
        {
            get { return _insertedNumber; }
            set
            {
                Number.Foreground = new SolidColorBrush(_app.Settings.Theme.FontColor);
                if (value.HasValue)
                {
                    _insertedNumber = value.Value;
                    Number.Text = value.Value.ToString();
                    NotePad.Opacity = 0.4;
                }
                else
                {
                    _insertedNumber = null;
                    Number.Text = String.Empty;
                    NotePad.Opacity = 1;
                }
            }
        }

        private List<string> _insertedNotes = new List<string>(9);
        public List<string> InsertedNotes
        {
            get { return _insertedNotes; }
            set
            {
                _insertedNotes = value;
                SetNoteVisibilities();
            }
        }

        public ContentCellControl(int size, int? value)
        {
            InitializeComponent();
            Initialize(size, value);
        }

        private void Initialize(int size, int? value)
        {
            Border.Width = LayoutRoot.Height = size;
            Border.Background = new SolidColorBrush(_app.Settings.Theme.LighterBackgroundColor);
            Border.BorderBrush = new SolidColorBrush(_app.Settings.Theme.BorderColor);
            Number.Foreground = new SolidColorBrush(_app.Settings.Theme.FontColor);
            Number.FontSize = size / 1.7;
            foreach (object obj in NotePad.Children)
            {
                TextBlock tb = (TextBlock)obj;
                tb.FontSize = size / 3.8;
                tb.Foreground = new SolidColorBrush(_app.Settings.Theme.FontColor);
            }
            if (value.HasValue) InsertedNumber = value.Value;
        }

        private void UserControl_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NumberPadRequested(this, null);
        }

        private void UserControl_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NotePadRequested(this, null);
        }

        private void SetNoteVisibilities()
        {
            foreach (object obj in NotePad.Children)
            {
                TextBlock tb = (TextBlock)obj;
                tb.Visibility = InsertedNotes.Contains(tb.Text) ? Visibility.Visible : Visibility.Collapsed;
            }
        }
    }
}
