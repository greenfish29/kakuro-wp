﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Kakuro.WP.Controls
{
    public partial class EmptyCellControl : UserControl
    {
        private App _app = (App)Application.Current;

        public EmptyCellControl(int size)
        {
            InitializeComponent();
            LayoutRoot.Background = new SolidColorBrush(_app.Settings.Theme.DarkerBackgroundColor);
            LayoutRoot.BorderBrush = new SolidColorBrush(_app.Settings.Theme.BorderColor);
            Width = Height = size;
        }
    }
}
