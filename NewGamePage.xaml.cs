﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Kakuro.WP.Models;
using Kakuro.WP.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Kakuro.WP
{
    public partial class NewGamePage : PhoneApplicationPage
    {
        private App _app = (App)Application.Current;
        private bool _pageCreated;
        private static Dictionary<string, string> _sizes = new Dictionary<string, string>
        {
            {"Small", AppResources.Small},
            {"Normal", AppResources.Normal},
            {"Big", AppResources.Big},
            {"Giant", AppResources.Giant},
        };

        public NewGamePage()
        {
            InitializeComponent();
            InitAppbar();
            SizePicker.ItemsSource = _sizes.Values;
            DifficultyPicker.ItemsSource = Settings.Difficulties.Values;
            _pageCreated = true;
        }

        private void InitAppbar()
        {
            ApplicationBar appbar = new ApplicationBar();
            appbar.BackgroundColor = (Color)Application.Current.Resources["PhoneBackgroundColor"];

            ApplicationBarIconButton startBtn = new ApplicationBarIconButton(new Uri("/Images/ok.png", UriKind.Relative));
            startBtn.Text = AppResources.StartGame;
            startBtn.Click += new EventHandler(delegate
            {
                NavigationService.Navigate(new Uri("/GamePage.xaml", UriKind.Relative));
                startBtn.IsEnabled = false;
                DataStore.ClearSettings();
            });
            appbar.Buttons.Add(startBtn);

            ApplicationBar = appbar;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (_pageCreated)
            {
                _pageCreated = false;
                SizePicker.SelectedItem = _sizes[_app.Settings.Size];
                DifficultyPicker.SelectedItem = Settings.Difficulties[_app.Settings.Difficulty];
            }
        }

        protected override void OnNavigatingFrom(System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            _app.Settings.Size = _sizes.Keys.ToList()[SizePicker.SelectedIndex];
            _app.Settings.Difficulty = Settings.Difficulties.Keys.ToList()[DifficultyPicker.SelectedIndex];
        }
    }
}