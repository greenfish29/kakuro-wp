﻿using System;
using System.IO.IsolatedStorage;
using System.Linq;

namespace Kakuro.WP.Models
{
    public static class DataStore
    {
        private const string SETTINGS_KEY = "kakuro.settings";
        private static readonly IsolatedStorageSettings _appSettings = IsolatedStorageSettings.ApplicationSettings;

        private static Settings _settings;
        public static Settings Settings
        {
            get
            {
                if (_settings == null)
                {
                    if (_appSettings.Contains(SETTINGS_KEY))
                    {
                        _settings = (Settings)_appSettings[SETTINGS_KEY];
                    }
                    else
                    {
                        _settings = new Settings
                        {
                            Theme = Settings.themes["Blue"],
                            Difficulty = Settings.Difficulties.Keys.First(),
                            Size = Settings.Sizes.Keys.First(),
                        };
                    }
                }
                return _settings;
            }
        }

        public static void SaveSettings(Action errorCallback)
        {
            try
            {
                _appSettings[SETTINGS_KEY] = Settings;
                _appSettings.Save();
            }
            catch
            {
                errorCallback();
            }
        }

        public static void ClearSettings()
        {
            Settings.Errors = null;
            Settings.Notes = null;
            Settings.Board = null;
            Settings.Solution = null;
        }
    }
}
