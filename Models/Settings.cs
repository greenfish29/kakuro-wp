﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Kakuro.Logic;
using Kakuro.WP.Resources;

namespace Kakuro.WP.Models
{
    public class Settings
    {
        public Theme Theme { get; set; }
        public string Size { get; set; }
        public Difficulty Difficulty { get; set; }
        public Board Board { get; set; }
        public Board Solution { get; set; }
        public List<int> Errors { get; set; }

        private Dictionary<int, List<string>> _notes;
        public Dictionary<int, List<string>> Notes
        {
            get
            {
                if (_notes == null) return new Dictionary<int, List<string>>(9);

                return _notes;
            }
            set { _notes = value; }
        }
        public static Dictionary<string, Theme> themes = new Dictionary<string, Theme>
        {
            {"Blue", BlueTheme},
            {"Dark", DarkTheme},
            {"Olive Green", OliveGreenTheme},
            {"Rose", RoseTheme},
            {"Shades of Autumn", ShadesOfAutumnTheme},
        };

        public static Dictionary<Difficulty, string> Difficulties = new Dictionary<Difficulty, string>
        {
            {Difficulty.Easy, AppResources.Easy},
            {Difficulty.Medium, AppResources.Medium},
            {Difficulty.Hard, AppResources.Hard},
        };

        public static Dictionary<string, int[]> Sizes = new Dictionary<string, int[]>
        {
            {"Small", new int[] {6, 6}},
            {"Normal", new int[] {6, 8}},
            {"Big", new int[] {8, 8}},
            {"Giant", new int[] {8, 11}},
        };

        #region Themes
        
        public static Theme BlueTheme
        {
            get
            {
                return new Theme
                {
                    Name = "Blue",
                    LighterBackgroundColor = GetColorFromHexa("#FFB0C4DE"),
                    DarkerBackgroundColor = GetColorFromHexa("#FF6495ED"),
                    BorderColor = Colors.Black,
                    HeadFontColor = Colors.Black,
                    FontColor = Colors.Black,
                };
            }
        }

        public static Theme ShadesOfAutumnTheme
        {
            get
            {
                return new Theme
                {
                    Name = "Shades of Autumn",
                    LighterBackgroundColor = GetColorFromHexa("#FFC99F79"),
                    DarkerBackgroundColor = GetColorFromHexa("#FF866449"),
                    BorderColor = Colors.Black,
                    HeadFontColor = Colors.White,
                    FontColor = GetColorFromHexa("#FF544332"),
                };
            }
        }
        
        public static Theme OliveGreenTheme
        {
            get
            {
                return new Theme
                {
                    Name = "Olive Green",
                    LighterBackgroundColor = GetColorFromHexa("#FFECE9D8"),
                    DarkerBackgroundColor = GetColorFromHexa("#FFA7B57F"),
                    BorderColor = Colors.Black,
                    HeadFontColor = Colors.Black,
                    FontColor = GetColorFromHexa("#FF464E2C"),
                };
            }
        }
        
        public static Theme RoseTheme
        {
            get
            {
                return new Theme
                {
                    Name = "Rose",
                    LighterBackgroundColor = GetColorFromHexa("#FFCFAFB7"),
                    DarkerBackgroundColor = GetColorFromHexa("#FFA26675"),
                    BorderColor = Colors.Black,
                    HeadFontColor = Colors.White,
                    FontColor = GetColorFromHexa("#FF6D4143"),
                };
            }
        }
        
        public static Theme DarkTheme
        {
            get
            {
                return new Theme
                {
                    Name = "Dark",
                    LighterBackgroundColor = Colors.LightGray,
                    DarkerBackgroundColor = Colors.Black,
                    BorderColor = Colors.Gray,
                    HeadFontColor = Colors.White,
                    FontColor = Colors.Black,
                };
            }
        }

        #endregion

        #region Helper Methods

        //public static SolidColorBrush GetBrushFromHexa(string hexaColor)
        //{
        //    return new SolidColorBrush(
        //        Color.FromArgb(
        //            Convert.ToByte(hexaColor.Substring(1, 2), 16),
        //            Convert.ToByte(hexaColor.Substring(3, 2), 16),
        //            Convert.ToByte(hexaColor.Substring(5, 2), 16),
        //            Convert.ToByte(hexaColor.Substring(7, 2), 16)
        //        )
        //    );
        //}

        public static Color GetColorFromHexa(string hexaColor)
        {
            return Color.FromArgb(
                Convert.ToByte(hexaColor.Substring(1, 2), 16),
                Convert.ToByte(hexaColor.Substring(3, 2), 16),
                Convert.ToByte(hexaColor.Substring(5, 2), 16),
                Convert.ToByte(hexaColor.Substring(7, 2), 16)
            );
        }

        #endregion
    }
}