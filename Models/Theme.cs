﻿using System.Windows.Media;

namespace Kakuro.WP.Models
{
    public class Theme
    {
        public string Name { get; set; }
        // Background colors
        public Color DarkerBackgroundColor { get; set; }
        public Color LighterBackgroundColor { get; set; }
        // Font colors
        public Color HeadFontColor { get; set; }
        public Color FontColor { get; set; }
        // Border color
        public Color BorderColor { get; set; }
    }
}
