﻿using Kakuro.WP.Resources;

namespace Kakuro.WP
{
    public class LocalizedStrings
    {
        private static AppResources _localizedStrings = new AppResources();

        public AppResources LocalizedResources { get { return _localizedStrings; } }
    }
}
