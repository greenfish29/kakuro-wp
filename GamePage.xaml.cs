﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Navigation;
using Kakuro.Logic;
using Kakuro.WP.Controls;
using Kakuro.WP.Models;
using Kakuro.WP.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Kakuro.WP
{
    public partial class GamePage : PhoneApplicationPage
    {
        private readonly int DISPLAY_WIDTH = 480;
        private App _app = (App)Application.Current;
        private LogicManager _manager = LogicManager.Instance;
        private int? _activeCellIndex;
        public ContentCellControl ActiveCell
        {
            get
            {
                if (_activeCellIndex.HasValue)
                {
                    return (ContentCellControl)boardContainer.Children[_activeCellIndex.Value];
                }

                return null;
            }
        }
        private bool _gameOver;
        private bool _helpSolving;

        public GamePage()
        {
            InitializeComponent();
            NumberPad.NumberSelected += new EventHandler(NumberSelected);
            ApplicationBar appbar = new ApplicationBar();
            appbar.BackgroundColor = (Color)Application.Current.Resources["PhoneBackgroundColor"];
            ApplicationBar = appbar;
            SetDefaultAppbar();
            if(_app.Settings.Board == null)
                StartNewGame();
            else
                ResumeGame();
        }

        private void _manager_GameEnded(object sender, EventArgs e)
        {
            _gameOver = true;
            // disable some application bar buttons
            ((ApplicationBarIconButton)ApplicationBar.Buttons[0]).IsEnabled = false;
            ((ApplicationBarIconButton)ApplicationBar.Buttons[1]).IsEnabled = false;
            ((ApplicationBarIconButton)ApplicationBar.Buttons[2]).IsEnabled = false;
            // clear saved data
            DataStore.ClearSettings();
            // display dialog
            if (_helpSolving) return;

            Dispatcher.BeginInvoke(delegate()
            {
                MessageBox.Show(AppResources.Winner, AppResources.WinnerTitle, MessageBoxButton.OK);
            });
        }

        private void SetDefaultAppbar()
        {
            if(ApplicationBar.Buttons.Count > 0) ApplicationBar.Buttons.Clear();

            // Check button
            ApplicationBarIconButton checkButton = new ApplicationBarIconButton(new Uri("/Images/check.png", UriKind.Relative));
            checkButton.Text = AppResources.Check;
            checkButton.Click += new EventHandler(delegate
            {
                HighlightErrors(_manager.Check());
            });
            ApplicationBar.Buttons.Add(checkButton);

            // Solve button
            ApplicationBarIconButton solveButton = new ApplicationBarIconButton(new Uri("/Images/solve.png", UriKind.Relative));
            solveButton.Text = AppResources.Solve;
            solveButton.Click += new EventHandler(delegate
            {
                _helpSolving = true;

                _manager.Solve(true);
                foreach (object obj in boardContainer.Children)
                {
                    if (obj.GetType() == typeof(ContentCellControl))
                    {
                        ContentCellControl cell = (ContentCellControl)obj;
                        cell.InsertedNotes = new List<string>(9);
                    }
                }
                DrawBoard(_manager.Board, true);
            });
            ApplicationBar.Buttons.Add(solveButton);

            // Clear button
            ApplicationBarIconButton clearButton = new ApplicationBarIconButton(new Uri("/Images/clear.png", UriKind.Relative));
            clearButton.Text = AppResources.Clear;
            clearButton.Click += new EventHandler(delegate
            {
                NumberPad.SelectedNotes.Clear();
                foreach (object obj in boardContainer.Children)
                {
                    if (obj.GetType() == typeof(ContentCellControl))
                    {
                        ContentCellControl cell = (ContentCellControl)obj;
                        cell.InsertedNumber = null;
                        cell.InsertedNotes = new List<string>(9);
                        _manager.SetValue(boardContainer.Children.IndexOf(cell), null);
                    }
                }
                DrawBoard(_manager.Board, true);
            });
            ApplicationBar.Buttons.Add(clearButton);

            // Settings button
            ApplicationBarIconButton settingsButton = new ApplicationBarIconButton(new Uri("/Images/settings.png", UriKind.Relative));
            settingsButton.Text = AppResources.Settings;
            settingsButton.Click += new EventHandler(delegate
            {
                NavigationService.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
            });
            ApplicationBar.Buttons.Add(settingsButton);

            // About menu item
            ApplicationBarMenuItem aboutItem = new ApplicationBarMenuItem(AppResources.AboutPageTitle);
            aboutItem.Click += new EventHandler(delegate
            {
                NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
            });
            ApplicationBar.MenuItems.Add(aboutItem);
        }

        private void SetNumberAppbar()
        {
            if (ApplicationBar.Buttons.Count > 0) ApplicationBar.Buttons.Clear();
            if (ApplicationBar.MenuItems.Count > 0) ApplicationBar.MenuItems.Clear();

            // Clear button
            ApplicationBarIconButton clearButton = new ApplicationBarIconButton(new Uri("/Images/clear.png", UriKind.Relative));
            clearButton.Text = AppResources.Clear;
            clearButton.Click += new EventHandler(delegate
            {
                NumberPad.SelectedNumber = null;
                NumberSelected(null, null);
            });
            clearButton.IsEnabled = ActiveCell.InsertedNumber.HasValue ? true : false;
            ApplicationBar.Buttons.Add(clearButton);

            // Cancel button
            ApplicationBarIconButton cancelButton = new ApplicationBarIconButton(new Uri("/Images/cancel.png", UriKind.Relative));
            cancelButton.Text = AppResources.Cancel;
            cancelButton.Click += new EventHandler(delegate
            {
                NumberPad.Visibility = Visibility.Collapsed;
                SetDefaultAppbar();
            });
            ApplicationBar.Buttons.Add(cancelButton);
        }

        private void SetNoteModeAppbar()
        {
            if (ApplicationBar.Buttons.Count > 0) ApplicationBar.Buttons.Clear();
            if (ApplicationBar.MenuItems.Count > 0) ApplicationBar.MenuItems.Clear();

            // Ok button
            ApplicationBarIconButton okButton = new ApplicationBarIconButton(new Uri("/Images/ok.png", UriKind.Relative));
            okButton.Text = AppResources.Ok;
            okButton.Click += new EventHandler(delegate
            {
                ActiveCell.InsertedNotes = NumberPad.SelectedNotes;
                _activeCellIndex = null;
                NumberPad.Visibility = Visibility.Collapsed;
                SetDefaultAppbar();
            });
            ApplicationBar.Buttons.Add(okButton);

            // Cancel button
            ApplicationBarIconButton cancelButton = new ApplicationBarIconButton(new Uri("/Images/cancel.png", UriKind.Relative));
            cancelButton.Text = AppResources.Cancel;
            cancelButton.Click += new EventHandler(delegate
            {
                _activeCellIndex = null;
                NumberPad.Visibility = Visibility.Collapsed;
                SetDefaultAppbar();
            });
            ApplicationBar.Buttons.Add(cancelButton);
        }

        private void StartNewGame()
        {
            _gameOver= _helpSolving = false;

            int width = Settings.Sizes[_app.Settings.Size][0];
            int height = Settings.Sizes[_app.Settings.Size][1];
            Difficulty difficulty = _app.Settings.Difficulty;

            _manager.NewGame(width, height, difficulty);
        }

        private void ResumeGame()
        {
            _manager.Board = _app.Settings.Board;
            _manager.Solution = _app.Settings.Solution;
        }

        public void DrawBoard(Board board, bool drawingSolution = false)
        {
            if(drawingSolution)
            {
                foreach(Cell cell in board.Items)
                {
                    if (cell.CellType == CellType.Content)
                    {
                        ContentCellControl control = (ContentCellControl)boardContainer.Children[board.Items.IndexOf(cell)];
                        control.InsertedNumber = cell.Value;
                    }
                }
                return;
            }

            boardContainer.Children.Clear();
            int cellSize = DISPLAY_WIDTH / board.Width;
            boardContainer.Width = DISPLAY_WIDTH;

            foreach (Cell cell in board.Items)
            {
                switch (cell.CellType)
                {
                    case CellType.Summary:
                        boardContainer.Children.Add(new TotalsCellControl(cellSize, cell.RowSumValue, cell.ColSumValue));
                        break;
                    case CellType.Content:
                        boardContainer.Children.Add(new ContentCellControl(cellSize, cell.Value));
                        ContentCellControl control = (ContentCellControl)boardContainer.Children[boardContainer.Children.Count - 1];
                        control.InsertedNumber = cell.Value;
                        control.NumberPadRequested += new EventHandler(NumberPadRequested);
                        control.NotePadRequested += new EventHandler(NotePadRequested);
                        break;
                    case CellType.Empty:
                        boardContainer.Children.Add(new EmptyCellControl(cellSize));
                        break;
                }
            }
        }

        private void NumberPadRequested(object sender, EventArgs e)
        {
            if (_gameOver) return;
            _activeCellIndex = boardContainer.Children.IndexOf((ContentCellControl)sender);
            NumberPad.Visibility = Visibility.Visible;
            NumberPad.ActiveMode = NumberPadControl.Mode.NUMBER;
            SetNumberAppbar();
        }

        private void NotePadRequested(object sender, EventArgs e)
        {
            if (_gameOver) return;
            _activeCellIndex = boardContainer.Children.IndexOf((ContentCellControl)sender);
            NumberPad.Visibility = Visibility.Visible;
            NumberPad.ActiveMode = NumberPadControl.Mode.NOTE;
            NumberPad.SelectedNotes = ActiveCell.InsertedNotes;
            SetNoteModeAppbar();
        }

        private void NumberSelected(object sender, EventArgs e)
        {
            ActiveCell.InsertedNumber = NumberPad.SelectedNumber;
            NumberPad.Visibility = Visibility.Collapsed;
            SetDefaultAppbar();

            _manager.SetValue(_activeCellIndex.Value, ActiveCell.InsertedNumber);

            _activeCellIndex = null;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            _manager.GameEnded += new EventHandler(_manager_GameEnded);

            DrawBoard(_manager.Board);
            HighlightErrors(_app.Settings.Errors);
            CheckNotes();
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            _manager.GameEnded -= new EventHandler(_manager_GameEnded);

            _app.Settings.Notes.Clear();

            if (!_gameOver)
            {
                _app.Settings.Errors = GetActualErrors();
                _app.Settings.Board = _manager.Board;
                _app.Settings.Solution = _manager.Solution;

                foreach (object obj in boardContainer.Children)
                {
                    if (obj.GetType() == typeof(ContentCellControl))
                    {
                        ContentCellControl cell = (ContentCellControl)obj;

                        if (cell.InsertedNotes.Count > 0)
                        {
                            Dictionary<int, List<string>> temp = new Dictionary<int, List<string>>(_app.Settings.Notes);
                            temp.Add(boardContainer.Children.IndexOf(cell), new List<string>(cell.InsertedNotes));
                            _app.Settings.Notes = temp;
                        }
                    }
                }
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);

            if (NavigationService.BackStack.First().Source.OriginalString == "/NewGamePage.xaml")
            {
                NavigationService.RemoveBackEntry();
            }
        }

        private void HighlightErrors(List<int> errors)
        {
            if (errors == null) return;

            //_errorsShowed = true;
            foreach (int i in errors)
            {
                ((ContentCellControl)boardContainer.Children[i]).Number.Foreground = new SolidColorBrush(Colors.Red);
            }
        }

        private void CheckNotes()
        {
            foreach (KeyValuePair<int,List<string>> k in _app.Settings.Notes)
            {
                ((ContentCellControl)boardContainer.Children[k.Key]).InsertedNotes = k.Value;
            }
        }

        private List<int> GetActualErrors()
        {
            List<int> errors = new List<int>();

            foreach (object obj in boardContainer.Children)
            {
                if (obj.GetType() == typeof(ContentCellControl))
                {
                    ContentCellControl cell = (ContentCellControl)obj;

                    if (((SolidColorBrush)cell.Number.Foreground).Color == Colors.Red)
                    {
                        errors.Add(boardContainer.Children.IndexOf(cell));
                    }
                }
            }

            if (errors.Count > 0)
                return errors;

            return null;
        }
    }
}