﻿using System.Windows;
using Kakuro.WP.Models;
using Microsoft.Phone.Controls;

namespace Kakuro.WP
{
    public partial class SettingsPage : PhoneApplicationPage
    {
        private App _app = (App)Application.Current;

        public SettingsPage()
        {
            InitializeComponent();
            DataContext = Settings.themes.Keys;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ThemePicker.SelectedItem = _app.Settings.Theme.Name;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            _app.Settings.Theme = Settings.themes[ThemePicker.SelectedItem.ToString()];
        }
    }
}